# KKY/PRJ4 Projekt 4

URL classification

## Requirements

Project is based on libraries

- [Jupyter](https://jupyter.org/)
- [wordninja](https://github.com/keredson/wordninja)
- [scipy](https://www.scipy.org/)
- [scikit-learn](https://scikit-learn.org/)

### [Anaconda](https://www.anaconda.com/)

Use `conda env export > environment.yml` to export environment.

Use `conda env create -f environment.yml` to import environment.

To activate the environment, use `activate prj4`.

To deactivate an active environment, use `deactivate`.

### pip

Use `pip freeze > requirements.txt` to export environment.

Use `pip install -r requirements.txt` to import environment.

To activate the environment use `source env/bin/activate` (on macOS and Linux) or `.\env\Scripts\activate` (on Windows).

More info about `pip` environments [here](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/).

More info about `pip` in generall [here](https://pip.pypa.io/en/stable/user_guide/).

## Run

- `classification.ipynb`
- `preprocessing.ipynb`

Two files, data preprocessing and classification.
Just open in your Jupyter and enjoy!

```sh
$ jupyter notebook
```

## Dataset

Dataset is combined from two sources:

1. https://www.kaggle.com/antonyj453/urldataset
2. https://github.com/StevenBlack/hosts
